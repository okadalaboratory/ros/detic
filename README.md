# Detic

## Deticとは
DeticはFacebook Researchによって開発された、オブジェクト検出に特化したセグメンテーションモデルです。このモデルは2022年に公開され、21,000種類以上のオブジェクトクラスを高い精度で識別できることが特徴です。Deticは、これまで検出が困難だったオブジェクトの検出にも優れています。

Deticの目立った特徴の一つは、モデルが再トレーニングを必要としない点です。これにより、効率的で時短につながるソリューションとなっています。従来のオブジェクト検出モデルは、ローカライゼーション（オブジェクトの位置を特定すること）とクラス分類を組み合わせて実行しますが、Deticはこれらを切り離すことで、多様なオブジェクトを正確に識別し分類できるようになっています。

Deticは、21,000クラスのImageNetデータセットの全クラスに対するディテクターをトレーニングする最初のモデルです。このモデルは、非常に汎用性が高く、幅広いタスクに適しています。さらに、Deticのアーキテクチャは、データセット特有のオブジェクト検出用に設計されており、Weakly-Supervised Object Detection (WSOD)を使用しています。これにより、バウンディングボックス情報に頼らずにオブジェクトディテクターをトレーニングすることができます。

Deticの柔軟性のある利点の一つは、微調整によって特定のオブジェクトの詳細な検出に優れることです。さらに、CLIP埋め込みを活用することで、ゼロショット検出タスクにおいても優れた能力を発揮します​

[オリジナル論文](https://arxiv.org/abs/2201.02605)

[公式の実装](https://github.com/facebookresearch/Detic)

##
Detic は
- 物体検出用のデータセットで「何が・どこに映っているか」を学習し
- 画像分類用のデータセットで「何が映っているか」のみを学習します。

これにより、画像分類用のデータセット分、多くのデータを用いてモデルを作成でき、結果として2万種類という多くの物体を検出することが可能となりました。


## HomeRobot: Open-Vocabulary Mobile Manipulation
https://github.com/shiyoung77/OVIR-3D


## 参考にした

[Google colabでDeticを動かす](https://qiita.com/hkwsdgea_ttt2/items/26512867c036abbc3a47)

[Deticのデモ](https://colab.research.google.com/drive/1QtTW9-ukX2HKZGvt0QvVGqjuqEykoZKI#scrollTo=S7MpTaZ0YNTb)

[](https://axross-recipe.com/recipes/433)

## 
[阪大原田研 detic_ros](https://github.com/Osaka-University-Harada-Laboratory/detic_ros)

[eR@sers detic-ros-docker](https://gitlab.com/trcp/erasers_ws/-/tree/master/detic-ros-docker?ref_type=heads)

